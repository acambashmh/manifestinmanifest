var express = require('express');
var app = express();
var fetch = require('isomorphic-fetch');
var Promise = require('bluebird');
var getDownloadManifests = require('./getDownloadManifests');


app.use(express.static('./'));

app.get('/', function (req, res) {
    res.send('Hello World!');
});


app.get('/test', function (req, res) {
    getDownloadManifests('http://localhost:3000/manifest1.json')
    .then(function(data){
        res.json(data);
    })
});


app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});
