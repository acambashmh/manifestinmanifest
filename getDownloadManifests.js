
function getAllManifests(path) {
     return getManifest(path)
     .then(function(data){
         var manifestInseception = [];
         data.download_packages.downloads.forEach(function(downloadItem){
             if(isItemManifest(downloadItem)){
                 manifestInseception.push(downloadItem);
             }
         });
         
         if(manifestInseception.length === 0){
             return [data];
         }else{
             var manifestInseceptionPromises = manifestInseception.map(function(item){
                return getAllManifests(item.package_details.download_uri.relative_url);
             });
             return Promise.all(manifestInseceptionPromises).then(function(items){
                 var temp = items.reduce(function(curr,next){
                     return curr.concat(next);
                 }, []);
                 return temp.concat([data]);
             })
         }
     })
}

function isItemManifest(downloadItem) {
    if(downloadItem.type === 'download-manifest'){
        return true;
    }
    return false;
}

function getManifest(path) {
    return fetch(path)
        .then(function (response) {
            if (response.status >= 400) {
                throw new Error("Bad response from server");
            }
            return response.json();
        })
        .then(function (data) {
            return data
        })
}

module.exports = getAllManifests;